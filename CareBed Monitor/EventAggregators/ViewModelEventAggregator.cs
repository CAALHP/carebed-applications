﻿using Caliburn.Micro;

namespace CareBedMonitor.EventAggregators
{
    public class ViewModelEventAggregator
    {
        static EventAggregator eventAggregator;

        public static EventAggregator EventAggregator
        {
            get { return eventAggregator ?? (eventAggregator = new EventAggregator()); }
        }

    }
}
