﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareBedMonitor.Model;
using CareBedMonitor.ViewModels;

namespace CareBedMonitor.Dialogs.ViewModels
{
    public class AddUserDialogViewModel: BaseViewModel
    {
        private string _userName;
        private CareStoreUserLevel _userLevel;
        private TouchKeyboardViewModel _touchKeyboard = new TouchKeyboardViewModel();
        private UserDbManager _dbManager;

        public AddUserDialogViewModel(UserDbManager dbManager)
        {
            this._dbManager = dbManager;
        }


        public string UserName
        {
            get { return _userName; }
            set
            {
                if (value == _userName) return;
                _userName = value;
                NotifyOfPropertyChange(() => UserName);
                NotifyOfPropertyChange(() => CanStore);
            }
        }

        public bool CanStore
        {
            get { return !string.IsNullOrEmpty(UserName); }
        }

        public void Store()
        {
            var id = Guid.NewGuid().ToString();
            var creationTime = DateTime.Now.ToUniversalTime().ToString(CultureInfo.InvariantCulture);

            var profile = new UserProfile()
                {
                    UserId = id,
                    CreationTime = creationTime,
                    FullName = UserName,
                    UserName = UserName,
                    UserLevel = UserLevel
                };

            _dbManager.AddEntry(profile);

            //TODO consider sending this to caalhp, or change this dialog to startup UserManager insted.

            TryClose();
        }

        public void Cancel()
        {
            TryClose();
        }

        public TouchKeyboardViewModel TouchKeyboard
        {
            get { return _touchKeyboard; }
            set
            {
                _touchKeyboard = value;
                NotifyOfPropertyChange(() => TouchKeyboard);
            }
        }

        public CareStoreUserLevel SelectedEnumType
        {
            get { return UserLevel; }
            set
            {
                if (value == UserLevel) return;
                UserLevel = value;
                NotifyOfPropertyChange(() => SelectedEnumType);
            }
        }

        public CareStoreUserLevel UserLevel
        {
            get { return _userLevel; }
            set
            {
                if (value == _userLevel) return;
                _userLevel = value;
                NotifyOfPropertyChange(() => UserLevel);
            }
        }

        public IEnumerable<CareStoreUserLevel> CareStoreUserLevelsEnumTypeValues
        {
            get
            {
                return Enum.GetValues(typeof(CareStoreUserLevel))
                    .Cast<CareStoreUserLevel>();
            }
        }

        public override void Dispose()
        {
            
        }
    }
}
