﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareBedMonitor.EventAggregators;
using CareBedMonitor.EventMessages;
using CareBedMonitor.Model;
using CareBedMonitor.ViewModels;

namespace CareBedMonitor.Dialogs.ViewModels
{
    public class ChangeUserDialogViewModel: BaseViewModel
    {

        private List<UserProfile> _userList;
        private UserProfile _selectedUser;

        public ChangeUserDialogViewModel(UserDbManager dbManager)
        {
            UserList = dbManager.Users;

            try
            {
                var currentUserId = ConfigurationManager.AppSettings.Get("currentUser");
                if (!string.IsNullOrEmpty(currentUserId))
                    SelectedUser = UserList.Single(x => x.UserId == currentUserId);
            }
            catch (Exception)
            {
                SelectedUser = null;
            }
        }

        public UserProfile SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                if (Equals(value, _selectedUser)) return;
                _selectedUser = value;
                NotifyOfPropertyChange(() => SelectedUser);
            }
        }


        public List<UserProfile> UserList
        {
            get { return _userList; }
            set
            {
                if (Equals(value, _userList)) return;
                _userList = value;
                NotifyOfPropertyChange(() => UserList);
            }
        }

        public void Store()
        {
            bool result = false;

            if (SelectedUser.UserId != ConfigurationManager.AppSettings.Get("currentUser"))
            {
                Configuration config =
                    ConfigurationManager.OpenExeConfiguration
                        (ConfigurationUserLevel.None);

                //sets the center name in the app config to be used when creating identification
                config.AppSettings.Settings["currentUser"].Value = _selectedUser.UserId;
                //saves only the changes values in the config xml doc.
                //config.Save(ConfigurationSaveMode.Modified);
                config.Save();
                //force config manager to refresh new values, else changes wont be detected at runtime.
                ConfigurationManager.RefreshSection("appSettings");
                result = true;

                Utility.UserName = _selectedUser.FullName;

                ViewModelEventAggregator.EventAggregator.Publish( new ChangeCurrentUserMessage(){User = SelectedUser});
            }

            TryClose(result);
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public override void Dispose()
        {
            
        }
    }
}
