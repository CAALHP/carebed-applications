﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareBedMonitor.ViewModels;

namespace CareBedMonitor.Dialogs.ViewModels
{
    public class SetRoomIdDialogViewModel: BaseViewModel
    {
        #region fields

        private string _roomId;
        private TouchKeyboardViewModel _touchKeyboard = new TouchKeyboardViewModel();

        #endregion

        public SetRoomIdDialogViewModel()
        {
            _touchKeyboard.NumericLayout(true);
            try
            {
                RoomId = ConfigurationManager.AppSettings.Get("room");
            }
            catch (Exception)
            {
                
                
            }
            
        }

        #region properties

        public bool CanStore
        {
            get { return !string.IsNullOrEmpty(_roomId); }
        }


        public TouchKeyboardViewModel TouchKeyboard
        {
            get { return _touchKeyboard; }
            set
            {
                _touchKeyboard = value;
                NotifyOfPropertyChange(() => TouchKeyboard);
            }
        }

        public string RoomId
        {
            get { return _roomId; }
            set
            {
                if (value == _roomId) return;
                _roomId = value;
                NotifyOfPropertyChange(() => RoomId);
                NotifyOfPropertyChange(() => CanStore);
            }
        }

        #endregion

        #region Methods

        public void Store()
        {
            Configuration config =
                ConfigurationManager.OpenExeConfiguration
                    (ConfigurationUserLevel.None);

            //sets the center name in the app config to be used when creating identification
            config.AppSettings.Settings["room"].Value = _roomId;
            //saves only the changes values in the config xml doc.
            //config.Save(ConfigurationSaveMode.Modified);
            config.Save();
            //force config manager to refresh new values, else changes wont be detected at runtime.
            ConfigurationManager.RefreshSection("appSettings");

            TryClose();

        }

        #endregion

        public override void Dispose()
        {
            
        }
    }
}
