using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using CareBedMonitor.ViewModels;
using System;
using System.Collections.Generic;
using Caliburn.Micro;

namespace CareBedMonitor
{
    //public class AppBootstrapper : BootstrapperBase
    //{
    //    SimpleContainer _container;

    //    public AppBootstrapper()
    //    {
    //        Start();
    //    }

    //    protected override void Configure()
    //    {
    //        _container = new SimpleContainer();

    //        _container.Singleton<IWindowManager, WindowManager>();
    //        _container.Singleton<IEventAggregator, EventAggregator>();
    //        _container.PerRequest<IShell, ShellViewModel>();
    //    }

    //    protected override object GetInstance(Type service, string key)
    //    {
    //        var instance = _container.GetInstance(service, key);
    //        if (instance != null)
    //            return instance;

    //        throw new InvalidOperationException("Could not locate any instances.");
    //    }

    //    protected override IEnumerable<object> GetAllInstances(Type service)
    //    {
    //        return _container.GetAllInstances(service);
    //    }

    //    protected override void BuildUp(object instance)
    //    {
    //        _container.BuildUp(instance);
    //    }

    //    protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
    //    {
    //        DisplayRootViewFor<IShell>();
    //    }
	//}

    public class AppBootstrapper : Bootstrapper<IShell>
    {
        private CompositionContainer container;

        protected override void Configure()
        {
            container = new CompositionContainer(new AggregateCatalog(AssemblySource.Instance.Select(x => new AssemblyCatalog(x)).OfType<ComposablePartCatalog>()));

            CompositionBatch batch = new CompositionBatch();

            batch.AddExportedValue<IWindowManager>(new WindowManager());
            batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            batch.AddExportedValue(container);

            container.Compose(batch);
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            string contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(serviceType) : key;
            var exports = container.GetExportedValues<object>(contract);

            if (exports.Count() > 0)
            {
                return exports.First();
            }

            throw new Exception(string.Format("Could not locate any instances of contract {0}.", contract));
        }
    }
}