﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CareBedMonitor.EventAggregators;
using CareBedMonitor.EventMessages;
using CareBedMonitor.ViewModels;

namespace CareBedMonitor
{
    public class CareBedMonitorImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private const string Appname = "CareBed Monitor";
        private ShellViewModel _shell;

        public CareBedMonitorImplementation(ShellViewModel shell)
        {
            _shell = shell;
        }


        public string GetName()
        {
            return Appname;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            //Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListResponseEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserUpdateCompleteEvent)), _processId);

            RequestUserListFromCaalhp();
        }

        private void DispatchToApp(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }

        public void Show()
        {
            DispatchToApp(() =>
            {
                //DoTheMessagePump();
                var window = Application.Current.MainWindow;
                if (!window.IsVisible)
                {
                    window.Show();
                }
                window.WindowState = WindowState.Maximized;
                window.Activate();
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();
                //DoTheMessagePump();
            });
        }

        private void RequestUserListFromCaalhp()
        {
            var reqEvent = new UserListRequestEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, reqEvent);
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(GetName()))
            {
                //Show homescreen

                Show();
            }
        }

        private void HandleEvent(UserLoggedInEvent e)
        {
        }

        private void HandleEvent(UserLoggedOutEvent e)
        {
        }

        private void HandleEvent(UserListResponseEvent e)
        {
            ForwardUserList(e.Users);
        }

        private void HandleEvent(UserUpdateCompleteEvent e)
        {
            ForwardUserList(e.UserList);
        }

        private void ForwardUserList(List<UserProfile> uList)
        {
            ViewModelEventAggregator.EventAggregator.Publish( new CaalhpUserListRecievedMessage(){UserList = uList} );
        }
    }
}
