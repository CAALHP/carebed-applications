﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CareBedMonitor.EventMessages
{
    public class CaalhpUserLoggedInMessage: BaseMessage
    {
        public UserProfile User { get; set; }
    }
}
