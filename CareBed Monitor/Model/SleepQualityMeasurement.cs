﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareBedMonitor.Model
{
    [Serializable]
    public class SleepQualityMeasurement
    {
        public SleepQualityMeasurement(int value, DateTime timeStamp)
        {
            TimeStamp = timeStamp;
            Value = value;
        }

        public int Value { get; private set; }
        public DateTime TimeStamp { get; private set; }
    }
}
