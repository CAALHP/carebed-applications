﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareBedMonitor.Model
{
    [Serializable]
    public class WeightMeasurement
    {
        public WeightMeasurement(double value, DateTime timeStamp)
        {
            TimeStamp = timeStamp;
            Value = value;
        }

        public double Value { get; private set; }
        public DateTime TimeStamp { get; private set; }
    }
}
