﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CareBedMonitor.Model
{
    internal static class Utility
    {

        public static string CamId { get; set; }
        public static string MonitorId { get; set; }
        public static string RoomId { get; set; }
        public static string UserName { get; set; }

        #region Methods

        public static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion
    }
}
