﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CAALHP.Events.Types;

namespace CareBedMonitor.Model
{
    public class UserDbManager
    {
        private List<UserProfile> _users;
        private readonly string _filename = Utility.AssemblyDirectory + "\\UserData.dat";
        private static object _fileLock = new object();

        public UserDbManager()
        {
            //Check for file name. then read from file, if not possible make new list.
            //_filename = Utility.AssemblyDirectory + "\\facialdata.dat";
            try
            {
                if (File.Exists(_filename))
                    ReadFromFile();
                else
                {
                    _users = new List<UserProfile>();
                    //WritetoFile();
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<UserProfile> Users
        {
            get { return _users; }
        }

        public void AddEntry(UserProfile u)
        {
            _users.Add(u);

            WritetoFile();
        }

        /// <summary>
        /// Updates entrys that already exsist, adds entrys that miss.
        /// </summary>
        /// <param name="uList"></param>
        public void UpdateEntrys(List<UserProfile> uList)
        {


            var listWithupdates = uList;

            foreach (var entry in listWithupdates)
            {
                var oldUser = _users.FirstOrDefault(x => x.UserId == entry.UserId);
                if (oldUser != null)
                {
                    int index = _users.IndexOf(oldUser);
                    _users[index] = entry;
                }
                else
                {
                    _users.Add(entry);
                }
            }            

            WritetoFile();
        }

        public bool RemoveEntry(UserProfile u)
        {
            try
            {
                var entryToDelete = _users.Single(x => x.UserId == u.UserId);
                if (_users.Count > 0)
                    _users.Remove(entryToDelete);
              

                WritetoFile();

            }
            catch (Exception)
            {
                ReadFromFile();
                return false;
            }

            return true;
        }

        private void ReadFromFile()
        {

            try
            {
                lock (_fileLock)
                {
                    //using (Stream stream = File.Open(_filename, FileMode.Open))
                    //{
                    //    var bin = new BinaryFormatter();

                    //    _users = (List<UserProfile>)bin.Deserialize(stream);

                    //}

                    XmlSerializer deserializer = new XmlSerializer(typeof(List<UserProfile>));
                    TextReader textReader = new StreamReader(_filename, true);
                    //var usrlst = deserializer.Deserialize(textReader) as List<UserProfile>;
                    _users = deserializer.Deserialize(textReader) as List<UserProfile>;
                    textReader.Close();
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        private void WritetoFile()
        {
            try
            {

                lock (_fileLock)
                {
                    //using (Stream stream = File.Open(_filename, FileMode.Create))
                    //{
                    //    var bin = new BinaryFormatter();
                    //    bin.Serialize(stream, _users);
                    //} 

                    XmlSerializer serializer = new XmlSerializer(typeof(List<UserProfile>));

                    FileStream fs = new FileStream(_filename, FileMode.Create);
                    TextWriter writer = new StreamWriter(fs, new UTF8Encoding());
                    // Serialize using the XmlTextWriter.
                    serializer.Serialize(writer, _users);
                    writer.Close();
                }
            
            }
            catch (IOException)
            {
            }

        }
    }
}
