﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using CareBedDatatypeLib;
using CareBedMonitor.CareBedWeightSenserService;


namespace CareBedMonitor.Model
{
    public class BedDataService : IBedDataService
    {
         //private CareBedDataContractClient _bedService;
        private BedWeightSensorClient _bedService;

        public BedDataService()
        {


            try
            {
                var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("bedServiceAdress"));
               // var endpointadress = new EndpointAddress("http://cam.cw.consulo.dk:5079/CamControlService/");
                var httpBinding = new BasicHttpBinding { MaxReceivedMessageSize = 2147483647, MaxBufferSize = 2147483647 };
                _bedService = new BedWeightSensorClient(httpBinding, endpoint);
                
            }
            catch (Exception e)
            {

                throw;
            }

            //try
            //{
            //    //var maxsize = 2147483648;
            //    //binding
            //    var binding = new BasicHttpBinding();
            //    binding.MaxBufferSize = 2138483646;
            //    binding.MaxReceivedMessageSize = 2138483646;
            //    binding.SendTimeout = new System.TimeSpan(1, 0, 0, 0);
            //    binding.ReceiveTimeout = new System.TimeSpan(1, 0, 0, 0);
            //    //var binding = new BasicHttpsBinding();
            //    //endpoint
            //    var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("bedServiceAdress"));
            //    //<add key="marketplaceAddress" value="http://192.168.1.100/Marketplaceservice/CareStoreMarketplaceWcfService.svc"/>

            //    //channelfactory
            //    var channelFactory = new ChannelFactory<>(binding, endpoint);

            //    //todo _bedService = channelFactory.CreateChannel();
            //}
            //catch (Exception e)
            //{

            //    throw;
            //}
        }

        public bool RequestOccupied()
        {
            var result = _bedService.BedOccupied();
            return result ;
        }

        public double RequestWeight()
        {

            var result = _bedService.Weight();
            return result;
        }

        public int RequestSleepQuality()
        {
            var result = _bedService.SleepQuality();
            return result;
        }
    }
}
