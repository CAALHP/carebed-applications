﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using CareBedDatatypeLib;

namespace CareBedMonitor.Model
{
    public class CareBedWebDataService: ICareBedWebDataService
    {

        private ICareBedDataContract _webService;

        public CareBedWebDataService()
        {
            try
            {
                //var maxsize = 2147483648;
                //binding
                var binding = new BasicHttpBinding();
                binding.MaxBufferSize = 2138483646;
                binding.MaxReceivedMessageSize = 2138483646;
                binding.SendTimeout = new System.TimeSpan(1, 0, 0, 0);
                binding.ReceiveTimeout = new System.TimeSpan(1, 0, 0, 0);
                //var binding = new BasicHttpsBinding();
                //endpoint
                var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("webServiceAdress"));
                
                //channelfactory
                var channelFactory = new ChannelFactory<ICareBedDataContract>(binding, endpoint);

                _webService = channelFactory.CreateChannel();
            }
            catch (Exception e)
            {

                throw;
            }
        }



        public void UploadMonitorData(MonitorDataObject status)
        {
            try
            {
                if (status != null) 
                    _webService.UploadBedStatus(status);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
