﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareBedDatatypeLib;

namespace CareBedMonitor.Model
{
    [Serializable]
    public class UserDataModel
    {
        public UserDataModel(string userId)
        {
            UserId = userId;
            WeightMeasurements = new List<Measurement>();
            SleepQualityMeasurements = new List<SleepQualityMeasurement>();
        }

        public string UserId { get; set; }
        public List<Measurement> WeightMeasurements { get; set; }
        public List<SleepQualityMeasurement> SleepQualityMeasurements { get; set; } 
    }
}
