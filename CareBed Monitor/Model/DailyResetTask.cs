﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareBedMonitor.EventAggregators;
using CareBedMonitor.EventMessages;
using Quartz;

namespace CareBedMonitor.Model
{
    public class DailyResetTask: IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new DailyResetMessage() );
        }
    }
}
