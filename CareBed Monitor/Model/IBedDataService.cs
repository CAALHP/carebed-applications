﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareBedDatatypeLib;

namespace CareBedMonitor.Model
{
    public interface IBedDataService
    {
        bool RequestOccupied();
        double RequestWeight();
        int RequestSleepQuality();
    }
}
