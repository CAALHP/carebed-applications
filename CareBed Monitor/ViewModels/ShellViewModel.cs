using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using CAALHP.SOA.ICE.ClientAdapters;
using Caliburn.Micro;
using CareBedMonitor.Dialogs.ViewModels;
using CareBedMonitor.EventAggregators;
using CareBedMonitor.EventMessages;
using CareBedMonitor.Model;

namespace CareBedMonitor.ViewModels {

    [Export(typeof (IShell))]
    public class ShellViewModel : Conductor<Screen>.Collection.OneActive, IShell, IHandle<BaseMessage>
    {
        private CareBedMonitorImplementation _caalhpConctact;
        private AppAdapter _adapter; // DO NOT DELETE this is ment never to be used, just a placeholder.
        private const string endpoint = "localhost";
        private bool _connectedToCaalhp;
        private bool _isWindowEnabled;
        private string _header;
        private BaseViewModel _mainView;
        private System.Timers.Timer _statusTimer = new System.Timers.Timer(30000);
        private string _statusText;
        private ObservableCollection<BaseViewModel> _modules;
        private string _roomId;
        readonly IWindowManager _windowManager;


        [ImportingConstructor]
        public ShellViewModel(IWindowManager windowManager, IUserViewModel userview)
        {

            _windowManager = windowManager;

            InitCaalhp();

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            IsWindowEnabled = true;

            Header = "CareBed Monitor";

            _statusTimer.Elapsed += StatusTimerElapsed;

            var userbaseview = userview as UserViewModel;
            var viewmodellist = new List<BaseViewModel>() {userbaseview};

            InitModules(viewmodellist);

            
    //        Modules.Add(userbaseview);
//            NotifyOfPropertyChange(() => Modules);

        }

        public ObservableCollection<BaseViewModel> Modules
        {
            get { return _modules; }
            set
            {
                if (Equals(value, _modules)) return;
                _modules = value;
                NotifyOfPropertyChange(() => Modules);
            }
        }

        public string RoomId
        {
            get { return _roomId; }
            set
            {
                if (value == _roomId) return;
                _roomId = value;
                NotifyOfPropertyChange(() => RoomId);
            }
        }

        /// <summary>
        /// activation called from xaml databinding
        /// </summary>
        /// <param name="current"></param>
        public void ActivateCurrentModule(BaseViewModel current)
        {
            try
            {
                ActivateItem(current);
            }
            catch (Exception e)
            {
                //new Alarm(new HmiException("module", Resource.ModuleViewModel_ActivateCurrentModule_unable_to_display_the_module__ + current.Name, e));
                StatusText = "Failed loading view: " + current.Name;
            }
        }


        public BaseViewModel MainView
        {
            get { return _mainView; }
            set
            {
                if (Equals(value, _mainView)) return;
                _mainView = value;
                NotifyOfPropertyChange(() => MainView);
            }
        }

        public string Header
        {
            get { return _header; }
            set
            {
                if (value == _header) return;
                _header = value;
                NotifyOfPropertyChange(() => Header);
            }
        }

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (value == _statusText) return;
                _statusText = value;
                NotifyOfPropertyChange(() => StatusText);

                if (string.IsNullOrEmpty(value)) return;
                _statusTimer.Start();

            }
        }



        public bool IsWindowEnabled
        {
            get { return _isWindowEnabled; }
            set
            {
                if (value.Equals(_isWindowEnabled)) return;
                _isWindowEnabled = value;
                NotifyOfPropertyChange(() => IsWindowEnabled);
            }
        }

        public void WindowClose(object sender, CancelEventArgs e)
        {
            if (_connectedToCaalhp)
            {
                e.Cancel = true;
                //TODO check if this is needed 
                IsWindowEnabled = false;
            }
            else
            {
                foreach (var baseViewModel in Modules)
                {
                    baseViewModel.Dispose();
                }

                _statusTimer.Elapsed -= StatusTimerElapsed;
                _statusTimer.Stop();
            }
        }

        /// <summary>
        /// if center is not set, blocks program untill set.
        /// </summary>
        public void ViewLoaded()
        {
            LoadSettingsFromConfig();


            ////checks if the centername has been set in app.config, if null opens dialog to set value.
            //if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["room"])) return;

            //string result;
            //do
            //{
            //    //shows dialog for setcentername
            //    _windowManager.ShowDialog(new SetRoomIdDialogViewModel());
            //    result = ConfigurationManager.AppSettings["room"];

            //} while (string.IsNullOrEmpty(result));

            //SetRoomId();

        }

        private void LoadSettingsFromConfig()
        {
            var machineId = ConfigurationManager.AppSettings.Get("machineId");

            if (string.IsNullOrEmpty(machineId))
                Utility.MonitorId = SetMachineId();
            else
                Utility.MonitorId = machineId;

            SetRoomId();

            if (string.IsNullOrEmpty(RoomId))
                OpenSetRoomIdDialog();


            //Utility.UserName = ConfigurationManager.AppSettings.Get("currentUser");
            Utility.CamId = ConfigurationManager.AppSettings.Get("camId");


        }

        private void SetRoomId()
        {
            RoomId = ConfigurationManager.AppSettings.Get("room");

            Utility.RoomId = RoomId;
        }

        private string SetMachineId()
        {
            var machineId = Guid.NewGuid().ToString();

            Configuration config =
                ConfigurationManager.OpenExeConfiguration
                    (ConfigurationUserLevel.None);
            //Creates a new GUID as Id for the machine to be recognized by survayer along with room Id.
            config.AppSettings.Settings["machineId"].Value = machineId;
            //saves only the changes values in the config xml doc.
            //config.Save(ConfigurationSaveMode.Modified);
            config.Save();
            //force config manager to refresh new values, else changes wont be detected at runtime.
            ConfigurationManager.RefreshSection("appSettings");

            return machineId;
        }

        private void OpenSetRoomIdDialog()
        {

            string result;
            do
            {
                //shows dialog for setcentername
                _windowManager.ShowDialog(new SetRoomIdDialogViewModel());
                result = ConfigurationManager.AppSettings["room"];

            } while (string.IsNullOrEmpty(result));

            SetRoomId();
        }

        private void InitModules(IEnumerable<BaseViewModel> viewmodellist)
        {
            var homeView = new HomeViewModel();
            //var userView = new UserViewModel();
            Modules = new ObservableCollection<BaseViewModel> { homeView };
            foreach (var baseViewModel in viewmodellist)
            {
                Modules.Add(baseViewModel);
            }

            NotifyOfPropertyChange(() => Modules);

            ActivateItem(homeView);
        }


        private void InitCaalhp()
        {
            _connectedToCaalhp = false;
            try
            {
                Task.Run(() =>
                {
                    try
                    {
                        _caalhpConctact = new CareBedMonitorImplementation(this);
                        _adapter = new AppAdapter(endpoint, _caalhpConctact);

                        _connectedToCaalhp = true;

                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show(e.Message);
                        StatusText = "Failed to connect to Caalhp";
                        _connectedToCaalhp = false;
                    }
                });
            }
            catch (Exception)
            {
                _connectedToCaalhp = false;
                StatusText = "Failed to connect to Caalhp";
            }
        }

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            //todo something here to log errors


            ////e.Handled = true;
            ////MessageBox.Show(e.Exception.Message);

            ////forces thread to change to da-DK format for storing log.
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");

            ////writes unhandled exceptions into logfil and closes down application.
            //var path = @"c:\Log\Exceptions\";

            ////  if (Directory.Exists(path))
            //Directory.CreateDirectory(path);


            //string filename = path + DateTime.Now.Date.ToShortDateString() + ".txt";
            ////writes unhandled exceptions into logfil and closes down application.
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename, true))
            //{
            //    file.Write(DateTime.Now.ToShortTimeString() + " ");
            //    file.WriteLine(e.Exception.ToString());

            //}
        }

        void StatusTimerElapsed(object sender, ElapsedEventArgs e)
        {
            StatusText = null;
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is empty by design
        }

        private void HandleMessage(StatusMessage o)
        {
            StatusText = o.Message;
        }

        private void HandleMessage(RoomIdChangedMessage o)
        {
            SetRoomId();
        }
    }

}