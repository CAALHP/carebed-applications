﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace CareBedMonitor.ViewModels
{
    //[Export(typeof(BaseViewModel))]
    public abstract class BaseViewModel : Screen
    {
        private string _name;

        protected BaseViewModel()
        {

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public abstract void Dispose();


        /// <summary>
        /// updates the controls of the current view to syncronice with viewmodel
        /// Should be used if resources are used for localization in the viewmodel.
        /// </summary>
        public void UpdateView()
        {
            NotifyOfPropertyChange(string.Empty);
        }
    }
}
