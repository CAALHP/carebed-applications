﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using Caliburn.Micro;
using CareBedMonitor.Dialogs.ViewModels;
using CareBedMonitor.EventAggregators;
using CareBedMonitor.EventMessages;
using CareBedMonitor.Model;

namespace CareBedMonitor.ViewModels
{
    public interface IUserViewModel
    {
        string Name { get; set; }
    }

    [Export(typeof(IUserViewModel))]
    public class UserViewModel : BaseViewModel, IHandle<BaseMessage>, IUserViewModel
    {
        readonly IWindowManager _windowManager;
        private readonly UserDbManager _dbManager;
        private UserProfile _currentUser;


        [ImportingConstructor]
        public UserViewModel(IWindowManager windowManager)
        {
            _dbManager = new UserDbManager();

            _windowManager = windowManager;

            Name = "User Settings";

            try
            {
                var currentUserId = ConfigurationManager.AppSettings.Get("currentUser");
                if (!string.IsNullOrEmpty(currentUserId))
                    _currentUser = _dbManager.Users.Single(x => x.UserId == currentUserId);

                Utility.UserName = _currentUser.FullName;
            }
            catch (Exception)
            {
                _currentUser = null;
            }

            ViewModelEventAggregator.EventAggregator.Subscribe(this);
        }

        //public string Name { get; set; }

        public string UserName
        {
            get
            {
                return _currentUser != null ? _currentUser.FullName : "";
            }
        }

        public string UserLevel
        {
            get
            {
                return _currentUser != null ? _currentUser.UserLevel.ToString() : "";
            }
        }

        public void AddUser()
        {
            OpenAddUserDialog();
        }

        private void OpenAddUserDialog()
        {
            //todo maybe get return value from show dialog.
            _windowManager.ShowDialog(new AddUserDialogViewModel(_dbManager));

            //todo consider some kind of notification here.
        }

        public void ChangeCurrentUser()
        {
            var result = OpenChangeCurrentUserDialog();
        }

        private bool? OpenChangeCurrentUserDialog()
        {
            return _windowManager.ShowDialog(new ChangeUserDialogViewModel(_dbManager));
        }

        public void SetRoomId()
        {
            OpenSetRoomIdDialog();

            ViewModelEventAggregator.EventAggregator.Publish(new RoomIdChangedMessage());
        }

        private void OpenSetRoomIdDialog()
        {

            string result;
            do
            {
                //shows dialog for setcentername
                _windowManager.ShowDialog(new SetRoomIdDialogViewModel());
                result = ConfigurationManager.AppSettings["room"];

            } while (string.IsNullOrEmpty(result));
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is empty by design
        }

        private void HandleMessage(ChangeCurrentUserMessage o)
        {
            _currentUser = o.User;
            NotifyOfPropertyChange(() => UserName);
            NotifyOfPropertyChange(() => UserLevel);
        }

        private void HandleMessage(CaalhpUserListRecievedMessage o)
        {
            var updatedList = o.UserList;
            _dbManager.UpdateEntrys(updatedList);
        }


        public override void Dispose()
        {
            
        }
    }
}
