﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;
using System.Xml.Serialization;
using CAALHP.Events.Types;
using Caliburn.Micro;
using CareBedDatatypeLib;
using CareBedMonitor.EventAggregators;
using CareBedMonitor.EventMessages;
using CareBedMonitor.Model;
using Quartz;
using Quartz.Impl;
using Action = System.Action;

namespace CareBedMonitor.ViewModels
{
    public class HomeViewModel : BaseViewModel, IHandle<BaseMessage>, IDisposable
    {
        private IScheduler _scheduler;
        private IBedDataService _bedDataService;
        private ICareBedWebDataService _webDataService;
        private List<SleepQualityMeasurement> _sleepQualityBuffer = new List<SleepQualityMeasurement>();

        private UserDataModel _userData;

        private bool _bedOccupied;
        private double _lastWeightMeasured;
        private SleepQuality _quality;
        private DateTime _lastMeasurementTimeStamp;
        private Timer _updateDataTimer;
        private Timer _uploadDataTimer;
        private DateTime? _outOfBedTime;
        private int _outOfBedCount;
        private SleepQuality _twoDayAvarage;
        private SleepQuality _forteenDayAvarage;
        private double _weightAvarage;
        private bool _weightMeasurementTaken;
        private static object _fileLock = new object();

        //For test only

        private int _updateCount = 0;
        private double _combinedWeights;
        private int _forteenDayAvarageValue;
        private int _twoDayAvarageValue;

        public HomeViewModel()
        {
            Name = "Home";

            InitUserDataModel();

            _weightMeasurementTaken = false;

            OutOfBedCount = 0;

            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            try
            {
                _webDataService = new CareBedWebDataService();
            }
            catch (Exception)
            {

                _webDataService = null;
            }

            try
            {
                _bedDataService = new BedDataService();
            }
            catch (Exception)
            {

                _bedDataService = null;
            }


            SetupTimer();


            // TwoDayAvarage = SleepQuality.Low;
            // ForteenDayAvarage = SleepQuality.Medium;

            //UpdateData();

            InitScheduler();

        }

        private void InitUserDataModel()
        {

            string currentUserId = "";
            try
            {
                currentUserId = ConfigurationManager.AppSettings.Get("currentUser");
                if (!string.IsNullOrEmpty(currentUserId))
                    ReadFromFile(currentUserId);
            }
            catch (Exception)
            {

            }

            if (_userData == null)
                _userData = new UserDataModel(currentUserId);
        }


        public override void Dispose()
        {
            if (_scheduler != null)
            {
                _scheduler.Shutdown();
                _scheduler = null;
            }
                
        }

        public bool BedOccupied
        {
            get { return _bedOccupied; }
            set
            {
                if (value.Equals(_bedOccupied)) return;
                _bedOccupied = value;
                NotifyOfPropertyChange(() => BedOccupied);
            }
        }

        public double WeightAvarage
        {
            get { return _weightAvarage; }
            set
            {
                if (value.Equals(_weightAvarage)) return;
                _weightAvarage = value;
                NotifyOfPropertyChange(() => WeightAvarage);
            }
        }

        public double LastWeightMeasured
        {
            get { return _lastWeightMeasured; }
            set
            {
                if (value.Equals(_lastWeightMeasured)) return;
                _lastWeightMeasured = value;
                NotifyOfPropertyChange(() => LastWeightMeasured);
            }
        }

        public SleepQuality Quality
        {
            get
            {
                //return _quality;
                return GetSleepQuality(SleepQuality);
            }
            //set
            //{
            //    if (value == _quality) return;
            //    _quality = value;
            //    NotifyOfPropertyChange(() => Quality);
            //}
        }

        public int SleepQuality { get; set; }

        public DateTime LastMeasurementTimeStamp
        {
            get { return _lastMeasurementTimeStamp; }
            set
            {
                if (value.Equals(_lastMeasurementTimeStamp)) return;
                _lastMeasurementTimeStamp = value;
                NotifyOfPropertyChange(() => LastMeasurementTimeStamp);
            }
        }

        public DateTime? OutOfBedTime
        {
            get { return _outOfBedTime; }
            set
            {
                if (value.Equals(_outOfBedTime)) return;
                _outOfBedTime = value;
                NotifyOfPropertyChange(() => OutOfBedTime);
            }
        }

        public int OutOfBedCount
        {
            get { return _outOfBedCount; }
            set
            {
                if (value == _outOfBedCount) return;
                _outOfBedCount = value;
                NotifyOfPropertyChange(() => OutOfBedCount);
            }
        }

        public int TwoDayAvarageValue
        {
            get { return _twoDayAvarageValue; }
            set
            {
                if (value == _twoDayAvarageValue) return;
                _twoDayAvarageValue = value;
                NotifyOfPropertyChange(() => TwoDayAvarageValue);
            }
        }

        public SleepQuality TwoHighLowIndicator
        {
            get
            {
                if (TwoDayAvarageValue <= 10 || TwoDayAvarageValue > 30 && TwoDayAvarageValue <= 40 || TwoDayAvarageValue > 70 && TwoDayAvarageValue <= 80)
                    return CareBedDatatypeLib.SleepQuality.Low;
                if (TwoDayAvarageValue > 20 && TwoDayAvarageValue <= 30 || TwoDayAvarageValue > 60 && TwoDayAvarageValue <= 70 || TwoDayAvarageValue > 90)
                    return CareBedDatatypeLib.SleepQuality.High;
                return CareBedDatatypeLib.SleepQuality.Medium;
            }
        }

        public SleepQuality TwoDayAvarage
        {
            get { return _twoDayAvarage; }
            set
            {
                if (value == _twoDayAvarage) return;
                _twoDayAvarage = value;
                NotifyOfPropertyChange(() => TwoDayAvarage);
                NotifyOfPropertyChange(() => TwoHighLowIndicator);
            }
        }

        public int ForteenDayAvarageValue
        {
            get { return _forteenDayAvarageValue; }
            set
            {
                if (value == _forteenDayAvarageValue) return;
                _forteenDayAvarageValue = value;
                NotifyOfPropertyChange(() => ForteenDayAvarageValue);
            }
        }

        public SleepQuality ForteenHighLowIndicator
        {
            get
            {
                if (ForteenDayAvarageValue <= 10 || ForteenDayAvarageValue > 30 && ForteenDayAvarageValue <= 40 || ForteenDayAvarageValue > 70 && ForteenDayAvarageValue <= 80)
                    return CareBedDatatypeLib.SleepQuality.Low;
                if (ForteenDayAvarageValue > 20 && ForteenDayAvarageValue <= 30 || ForteenDayAvarageValue > 60 && ForteenDayAvarageValue <= 70 || ForteenDayAvarageValue > 90)
                    return CareBedDatatypeLib.SleepQuality.High;
                return CareBedDatatypeLib.SleepQuality.Medium;
            }   
        }

        public SleepQuality ForteenDayAvarage
        {
            get { return _forteenDayAvarage; }
            set
            {
                if (value == _forteenDayAvarage) return;
                _forteenDayAvarage = value;
                NotifyOfPropertyChange(() => ForteenDayAvarage);
                NotifyOfPropertyChange(() => ForteenHighLowIndicator);
            }
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is empty by design
        }

        private void HandleMessage(DailyResetMessage o)
        {
            _weightMeasurementTaken = false;
        }

        private void SetupTimer()
        {

            double updateIntervel;
            double.TryParse(ConfigurationManager.AppSettings.Get("updateInterval"), out updateIntervel);

            //TODO removed this for testing only

            // updateIntervel = 5000;

            _updateDataTimer = new Timer(updateIntervel);
            _updateDataTimer.Elapsed += UpdateDataTimerOnTick;
            _updateDataTimer.Start();

            _uploadDataTimer = new Timer(30000);
            _uploadDataTimer.Elapsed += UploadDataTimerOnTick;
            _uploadDataTimer.Start();


        }

        private void UploadDataTimerOnTick(object sender, ElapsedEventArgs e)
        {

            try
            {
                if (_webDataService == null)
                    _webDataService = new CareBedWebDataService();
                Task.Run((Action)UploadData);
            }
            catch (Exception)
            {

                _webDataService = null;
            }

        }

        private void UpdateDataTimerOnTick(object sender, EventArgs e)
        {

            try
            {
                if (_bedDataService == null)
                    _bedDataService = new BedDataService();
                UpdateData();
            }
            catch (Exception)
            {
                _bedDataService = null;

                var msg = "Failed to upload to webserver";
                ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() { Message = msg });

            }
        }


        private void UpdateData()
        {


            var isBedOccupied = _bedDataService.RequestOccupied();

            if (isBedOccupied)
            {
                _updateCount++;

                BedOccupied = true;

                AddSleepQualityFromBedToBuffer();

                if (_updateCount == 60)
                {
                    UpdateSleepQuality();
                    _updateCount = 0;
                }

                if (!_weightMeasurementTaken)
                {
                    GetWeightMeasurementFromBed();
                }

            }
            else
            {
                if (BedOccupied)
                {
                    BedOccupied = false;
                    OutOfBedTime = DateTime.Now;

                    //TODO Reset each morning or something
                    OutOfBedCount = OutOfBedCount + 1;
                }
            }

        }

        private void AddSleepQualityFromBedToBuffer()
        {
            var newSleepQuality = _bedDataService.RequestSleepQuality();
            //Quality = GetSleepQuality(sleepQuality);

            if (_sleepQualityBuffer.Count == 180)
            {
                _sleepQualityBuffer.RemoveAt(0);
                _sleepQualityBuffer.Add(new SleepQualityMeasurement(newSleepQuality, DateTime.Now));
            }
            else
            {
                _sleepQualityBuffer.Add(new SleepQualityMeasurement(newSleepQuality, DateTime.Now));
            }


        }

        private void GetWeightMeasurementFromBed()
        {
            _weightMeasurementTaken = true;

            LastMeasurementTimeStamp = DateTime.Now;
            LastWeightMeasured = _bedDataService.RequestWeight();

            if (LastWeightMeasured != 0)
            {
                _userData.WeightMeasurements.Add(new Measurement()
                    {
                        Value = LastWeightMeasured,
                        TimeStamp = LastMeasurementTimeStamp
                    });

                WeightAvarage = Math.Round(_userData.WeightMeasurements.Average(x => x.Value), 1);
            }
            else
                _weightMeasurementTaken = false;
        }

        private void UpdateSleepQuality()
        {
            var timeRefrence = DateTime.Now.AddDays(-14);

            _userData.SleepQualityMeasurements.RemoveAll(x => x.TimeStamp < timeRefrence);

            SleepQuality = (int)Math.Round(_sleepQualityBuffer.Average(x => x.Value), 0);

            _userData.SleepQualityMeasurements.Add(new SleepQualityMeasurement(SleepQuality, DateTime.Now));
            

            //list.RemoveAll(r => list.Any(o => o != r && r.ID == o.otherID));
            UpdateAvarageSleepQuality();

            if (!string.IsNullOrEmpty(_userData.UserId))
                WritetoFile(_userData.UserId);

        }

        private void UpdateAvarageSleepQuality()
        {
            try
            {
                var twoDayTimeRef = DateTime.Now.AddDays(-2);

                var qualityTwoDaysOldMax = _userData.SleepQualityMeasurements.Where(x => x.TimeStamp > twoDayTimeRef).ToList();

                TwoDayAvarageValue = (int)Math.Round(qualityTwoDaysOldMax.Average(x => x.Value), 0);

                ForteenDayAvarageValue = (int)Math.Round(_userData.SleepQualityMeasurements.Average(x => x.Value), 0);

                TwoDayAvarage = GetSleepQuality(TwoDayAvarageValue);

                ForteenDayAvarage = GetSleepQuality(ForteenDayAvarageValue);

            }
            catch (Exception)
            {

                var msg = "Failed to update sleepquality avarage";
                ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() { Message = msg });
            }


            //(int)Math.Round( (List<SleepQualityMeasurement>)qualityTwoDaysOldMax.Average(x => x.), 0);


        }

        private void UploadData()
        {

            try
            {
                _webDataService.UploadMonitorData(new MonitorDataObject
                {
                    AvarageWeight = WeightAvarage,
                    BedStatus = BedOccupied,
                    CameraId = Utility.CamId,
                    RoomId = Utility.RoomId,
                    MachineId = Utility.MonitorId,
                    UserName = Utility.UserName,
                    LastWeighing = new Measurement() { Value = LastWeightMeasured, TimeStamp = LastMeasurementTimeStamp },
                    SleepQuality = SleepQuality

                });
            }
            catch (Exception)
            {

                var msg = "Failed to upload to webserver";
                ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() { Message = msg });
            }


        }

        private SleepQuality GetSleepQuality(int sleepQuality)
        {
            if (sleepQuality < 30)
                return CareBedDatatypeLib.SleepQuality.Low;
            if (sleepQuality < 70)
                return CareBedDatatypeLib.SleepQuality.Medium;

            return CareBedDatatypeLib.SleepQuality.High;
        }

        private void InitScheduler()
        {
           // IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();

            // and start it off
            _scheduler.Start();

            // define the job and tie it to DailyResetTask
            IJobDetail job = JobBuilder.Create<DailyResetTask>()
                .WithIdentity("job1", "group1")
                .Build();



            // Trigger the job to run at 05:00, and repeat every 24 hours.
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .StartAt(DateBuilder.DateOf(05, 00, 0))
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                    .RepeatForever())
                .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
        }


        private void ReadFromFile(string userId)
        {

            var fileName = Utility.AssemblyDirectory + "\\Data\\" + userId + ".dat";
            try
            {
                lock (_fileLock)
                {
                    using (Stream stream = File.Open(fileName, FileMode.Open))
                    {
                        var bin = new BinaryFormatter();

                        _userData = (UserDataModel)bin.Deserialize(stream);

                    }
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        private void WritetoFile(string userId)
        {

            var dir = Utility.AssemblyDirectory + "\\Data\\";
            var fileName = dir + userId + ".dat";

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            try
            {

                lock (_fileLock)
                {
                    using (Stream stream = File.Open(fileName, FileMode.Create))
                    {
                        var bin = new BinaryFormatter();
                        bin.Serialize(stream, _userData);
                    }
                }

            }
            catch (IOException)
            {
            }

        }

    }
}
