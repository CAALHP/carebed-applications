﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using CareBedWebcamLib.CamControlService;
using CareBedWebcamLib.CamImageService;

namespace CareBedWebcamLib
{
    public class CareCam
    {
        private CamControlServiceClient _control;
        private CamImageServiceClient _cam;
        private string _groupName;


        public CareCam(string serviceEndpoint, string groupName)
        {

            //   var endpoint = "http://cam.cw.consulo.dk:5080/CamImageService/";


            var endpointAdress = new EndpointAddress(serviceEndpoint);
            var httpBinding = new BasicHttpBinding { MaxReceivedMessageSize = 2147483647, MaxBufferSize = 2147483647 };
            _cam = new CamImageServiceClient(httpBinding, endpointAdress);

            try
            {
                var endpointadress = new EndpointAddress("http://cam.cw.consulo.dk:5079/CamControlService/");
                var httpBinding2 = new BasicHttpBinding { MaxReceivedMessageSize = 2147483647, MaxBufferSize = 2147483647 };
                _control = new CamControlServiceClient(httpBinding2,endpointadress);
            }
            catch (Exception e)
            {
                
                throw;
            }
            

            _groupName = groupName;

        }

        public BitmapSource GetLatestImage(string camId)
        {
            //groupname = "cwlab";
            //string id = "CY000001";
            string id = camId;
            
            string ftpuser;
            string ftppwd;
            string groupname;
            bool isRecMotion;
            string groupSID;
            ImageData[] thums;

            int returnCount;
            int totalCount;
            SeriesInfo[] seriesInfo;
            //ImageData imageData;


            var result = _control.GetCamInfo(id, out ftpuser, out ftppwd, out groupname, out isRecMotion);
            if (result.Success == false)
            {
                throw new Exception(result.ErrorMessage);
                //item = new DataItem(groupname);
                //callback(item, null);
            }

            //control.
            _control.GetGroupSID(_groupName, out groupSID);

            _cam.GetGroupSeriesInfo(groupSID, _groupName, id, 100, out returnCount, out totalCount, out seriesInfo);
            _cam.GetGroupSeriesInfo(groupSID, _groupName, null, 1000, out returnCount, out totalCount, out seriesInfo);

            foreach (var si in seriesInfo)
            {
                var count = si.ImageCount;
                // if (count == -1) continue;
                //var extension = info.ExtensionData;
                //var ex = info.
                var seriesID = si.SeriesId;
                

                //cam.GetImageThumbnails(groupSID, "CY000001", 
                _cam.GetImageThumbnails(groupSID, id, seriesID, 0, 1000, out returnCount, out thums);

                foreach (var info in thums)
                {
                    var array = info.Image;

                    using (var ms = new System.IO.MemoryStream(array))
                    {
                        var image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad; // here
                        image.StreamSource = ms;
                        image.EndInit();
                        return image;
                    }

                    //using (var stream = new MemoryStream(info.Image))
                    //{

                    //    var decoder = new JpegBitmapDecoder(stream,
                    //                                        BitmapCreateOptions.PreservePixelFormat,
                    //                                        BitmapCacheOption.OnLoad);
                        
                    //    return decoder.Frames[0];
                    //}
                }

            }

            return null;
        }

        //public BitmapImage[] GetImageSeries(string camId)
        //{

        //}

    }
}
