﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CareBedWebcamLib.CamControlService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GroupMode", Namespace="http://schemas.datacontract.org/2004/07/Cosesy.CameraServer.Data")]
    [System.SerializableAttribute()]
    public partial class GroupMode : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string GroupNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsRecMotionField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string GroupName {
            get {
                return this.GroupNameField;
            }
            set {
                if ((object.ReferenceEquals(this.GroupNameField, value) != true)) {
                    this.GroupNameField = value;
                    this.RaisePropertyChanged("GroupName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsRecMotion {
            get {
                return this.IsRecMotionField;
            }
            set {
                if ((this.IsRecMotionField.Equals(value) != true)) {
                    this.IsRecMotionField = value;
                    this.RaisePropertyChanged("IsRecMotion");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ResultInfo", Namespace="http://schemas.datacontract.org/2004/07/Cosesy.CameraServer.Data")]
    [System.SerializableAttribute()]
    public partial class ResultInfo : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorMessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool SuccessField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ErrorMessage {
            get {
                return this.ErrorMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorMessageField, value) != true)) {
                    this.ErrorMessageField = value;
                    this.RaisePropertyChanged("ErrorMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Success {
            get {
                return this.SuccessField;
            }
            set {
                if ((this.SuccessField.Equals(value) != true)) {
                    this.SuccessField = value;
                    this.RaisePropertyChanged("Success");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://mycosesy.com", ConfigurationName="CamControlService.ICamControlService")]
    public interface ICamControlService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/SetModes", ReplyAction="http://mycosesy.com/ICamControlService/SetModesResponse")]
        CareBedWebcamLib.CamControlService.ResultInfo SetModes(CareBedWebcamLib.CamControlService.GroupMode[] groupModes);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/SetModes", ReplyAction="http://mycosesy.com/ICamControlService/SetModesResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.ResultInfo> SetModesAsync(CareBedWebcamLib.CamControlService.GroupMode[] groupModes);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/IsCamEnrolled", ReplyAction="http://mycosesy.com/ICamControlService/IsCamEnrolledResponse")]
        CareBedWebcamLib.CamControlService.IsCamEnrolledResponse IsCamEnrolled(CareBedWebcamLib.CamControlService.IsCamEnrolledRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/IsCamEnrolled", ReplyAction="http://mycosesy.com/ICamControlService/IsCamEnrolledResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.IsCamEnrolledResponse> IsCamEnrolledAsync(CareBedWebcamLib.CamControlService.IsCamEnrolledRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/RemoveCamEnroll", ReplyAction="http://mycosesy.com/ICamControlService/RemoveCamEnrollResponse")]
        CareBedWebcamLib.CamControlService.ResultInfo RemoveCamEnroll(string camId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/RemoveCamEnroll", ReplyAction="http://mycosesy.com/ICamControlService/RemoveCamEnrollResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.ResultInfo> RemoveCamEnrollAsync(string camId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/EnrollCamInGroup", ReplyAction="http://mycosesy.com/ICamControlService/EnrollCamInGroupResponse")]
        CareBedWebcamLib.CamControlService.EnrollCamInGroupResponse EnrollCamInGroup(CareBedWebcamLib.CamControlService.EnrollCamInGroupRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/EnrollCamInGroup", ReplyAction="http://mycosesy.com/ICamControlService/EnrollCamInGroupResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.EnrollCamInGroupResponse> EnrollCamInGroupAsync(CareBedWebcamLib.CamControlService.EnrollCamInGroupRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/GetCamInfo", ReplyAction="http://mycosesy.com/ICamControlService/GetCamInfoResponse")]
        CareBedWebcamLib.CamControlService.GetCamInfoResponse GetCamInfo(CareBedWebcamLib.CamControlService.GetCamInfoRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/GetCamInfo", ReplyAction="http://mycosesy.com/ICamControlService/GetCamInfoResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.GetCamInfoResponse> GetCamInfoAsync(CareBedWebcamLib.CamControlService.GetCamInfoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/GetGroupSID", ReplyAction="http://mycosesy.com/ICamControlService/GetGroupSIDResponse")]
        CareBedWebcamLib.CamControlService.GetGroupSIDResponse GetGroupSID(CareBedWebcamLib.CamControlService.GetGroupSIDRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/GetGroupSID", ReplyAction="http://mycosesy.com/ICamControlService/GetGroupSIDResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.GetGroupSIDResponse> GetGroupSIDAsync(CareBedWebcamLib.CamControlService.GetGroupSIDRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/GetCamInfoByGuid", ReplyAction="http://mycosesy.com/ICamControlService/GetCamInfoByGuidResponse")]
        CareBedWebcamLib.CamControlService.GetCamInfoByGuidResponse GetCamInfoByGuid(CareBedWebcamLib.CamControlService.GetCamInfoByGuidRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/GetCamInfoByGuid", ReplyAction="http://mycosesy.com/ICamControlService/GetCamInfoByGuidResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.GetCamInfoByGuidResponse> GetCamInfoByGuidAsync(CareBedWebcamLib.CamControlService.GetCamInfoByGuidRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/SetCamFtpCredentials", ReplyAction="http://mycosesy.com/ICamControlService/SetCamFtpCredentialsResponse")]
        CareBedWebcamLib.CamControlService.ResultInfo SetCamFtpCredentials(string camId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://mycosesy.com/ICamControlService/SetCamFtpCredentials", ReplyAction="http://mycosesy.com/ICamControlService/SetCamFtpCredentialsResponse")]
        System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.ResultInfo> SetCamFtpCredentialsAsync(string camId);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="IsCamEnrolled", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class IsCamEnrolledRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public string camId;
        
        public IsCamEnrolledRequest() {
        }
        
        public IsCamEnrolledRequest(string camId) {
            this.camId = camId;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="IsCamEnrolledResponse", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class IsCamEnrolledResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public CareBedWebcamLib.CamControlService.ResultInfo IsCamEnrolledResult;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=1)]
        public bool wasEnrolled;
        
        public IsCamEnrolledResponse() {
        }
        
        public IsCamEnrolledResponse(CareBedWebcamLib.CamControlService.ResultInfo IsCamEnrolledResult, bool wasEnrolled) {
            this.IsCamEnrolledResult = IsCamEnrolledResult;
            this.wasEnrolled = wasEnrolled;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EnrollCamInGroup", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class EnrollCamInGroupRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public string camId;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=1)]
        public string groupName;
        
        public EnrollCamInGroupRequest() {
        }
        
        public EnrollCamInGroupRequest(string camId, string groupName) {
            this.camId = camId;
            this.groupName = groupName;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EnrollCamInGroupResponse", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class EnrollCamInGroupResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public CareBedWebcamLib.CamControlService.ResultInfo EnrollCamInGroupResult;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=1)]
        public System.Guid camEnrolledGuid;
        
        public EnrollCamInGroupResponse() {
        }
        
        public EnrollCamInGroupResponse(CareBedWebcamLib.CamControlService.ResultInfo EnrollCamInGroupResult, System.Guid camEnrolledGuid) {
            this.EnrollCamInGroupResult = EnrollCamInGroupResult;
            this.camEnrolledGuid = camEnrolledGuid;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetCamInfo", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class GetCamInfoRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public string camId;
        
        public GetCamInfoRequest() {
        }
        
        public GetCamInfoRequest(string camId) {
            this.camId = camId;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetCamInfoResponse", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class GetCamInfoResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public CareBedWebcamLib.CamControlService.ResultInfo GetCamInfoResult;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=1)]
        public string ftpUser;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=2)]
        public string ftpPwd;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=3)]
        public string groupName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=4)]
        public bool isRecMotion;
        
        public GetCamInfoResponse() {
        }
        
        public GetCamInfoResponse(CareBedWebcamLib.CamControlService.ResultInfo GetCamInfoResult, string ftpUser, string ftpPwd, string groupName, bool isRecMotion) {
            this.GetCamInfoResult = GetCamInfoResult;
            this.ftpUser = ftpUser;
            this.ftpPwd = ftpPwd;
            this.groupName = groupName;
            this.isRecMotion = isRecMotion;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetGroupSID", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class GetGroupSIDRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public string groupName;
        
        public GetGroupSIDRequest() {
        }
        
        public GetGroupSIDRequest(string groupName) {
            this.groupName = groupName;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetGroupSIDResponse", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class GetGroupSIDResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public CareBedWebcamLib.CamControlService.ResultInfo GetGroupSIDResult;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=1)]
        public string groupSID;
        
        public GetGroupSIDResponse() {
        }
        
        public GetGroupSIDResponse(CareBedWebcamLib.CamControlService.ResultInfo GetGroupSIDResult, string groupSID) {
            this.GetGroupSIDResult = GetGroupSIDResult;
            this.groupSID = groupSID;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetCamInfoByGuid", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class GetCamInfoByGuidRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public System.Guid camGuid;
        
        public GetCamInfoByGuidRequest() {
        }
        
        public GetCamInfoByGuidRequest(System.Guid camGuid) {
            this.camGuid = camGuid;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetCamInfoByGuidResponse", WrapperNamespace="http://mycosesy.com", IsWrapped=true)]
    public partial class GetCamInfoByGuidResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=0)]
        public CareBedWebcamLib.CamControlService.ResultInfo GetCamInfoByGuidResult;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=1)]
        public string camId;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=2)]
        public string ftpUser;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=3)]
        public string ftpPwd;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=4)]
        public string groupName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mycosesy.com", Order=5)]
        public bool isRecMotion;
        
        public GetCamInfoByGuidResponse() {
        }
        
        public GetCamInfoByGuidResponse(CareBedWebcamLib.CamControlService.ResultInfo GetCamInfoByGuidResult, string camId, string ftpUser, string ftpPwd, string groupName, bool isRecMotion) {
            this.GetCamInfoByGuidResult = GetCamInfoByGuidResult;
            this.camId = camId;
            this.ftpUser = ftpUser;
            this.ftpPwd = ftpPwd;
            this.groupName = groupName;
            this.isRecMotion = isRecMotion;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICamControlServiceChannel : CareBedWebcamLib.CamControlService.ICamControlService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CamControlServiceClient : System.ServiceModel.ClientBase<CareBedWebcamLib.CamControlService.ICamControlService>, CareBedWebcamLib.CamControlService.ICamControlService {
        
        public CamControlServiceClient() {
        }
        
        public CamControlServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CamControlServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CamControlServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CamControlServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo SetModes(CareBedWebcamLib.CamControlService.GroupMode[] groupModes) {
            return base.Channel.SetModes(groupModes);
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.ResultInfo> SetModesAsync(CareBedWebcamLib.CamControlService.GroupMode[] groupModes) {
            return base.Channel.SetModesAsync(groupModes);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CareBedWebcamLib.CamControlService.IsCamEnrolledResponse CareBedWebcamLib.CamControlService.ICamControlService.IsCamEnrolled(CareBedWebcamLib.CamControlService.IsCamEnrolledRequest request) {
            return base.Channel.IsCamEnrolled(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo IsCamEnrolled(string camId, out bool wasEnrolled) {
            CareBedWebcamLib.CamControlService.IsCamEnrolledRequest inValue = new CareBedWebcamLib.CamControlService.IsCamEnrolledRequest();
            inValue.camId = camId;
            CareBedWebcamLib.CamControlService.IsCamEnrolledResponse retVal = ((CareBedWebcamLib.CamControlService.ICamControlService)(this)).IsCamEnrolled(inValue);
            wasEnrolled = retVal.wasEnrolled;
            return retVal.IsCamEnrolledResult;
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.IsCamEnrolledResponse> IsCamEnrolledAsync(CareBedWebcamLib.CamControlService.IsCamEnrolledRequest request) {
            return base.Channel.IsCamEnrolledAsync(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo RemoveCamEnroll(string camId) {
            return base.Channel.RemoveCamEnroll(camId);
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.ResultInfo> RemoveCamEnrollAsync(string camId) {
            return base.Channel.RemoveCamEnrollAsync(camId);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CareBedWebcamLib.CamControlService.EnrollCamInGroupResponse CareBedWebcamLib.CamControlService.ICamControlService.EnrollCamInGroup(CareBedWebcamLib.CamControlService.EnrollCamInGroupRequest request) {
            return base.Channel.EnrollCamInGroup(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo EnrollCamInGroup(string camId, string groupName, out System.Guid camEnrolledGuid) {
            CareBedWebcamLib.CamControlService.EnrollCamInGroupRequest inValue = new CareBedWebcamLib.CamControlService.EnrollCamInGroupRequest();
            inValue.camId = camId;
            inValue.groupName = groupName;
            CareBedWebcamLib.CamControlService.EnrollCamInGroupResponse retVal = ((CareBedWebcamLib.CamControlService.ICamControlService)(this)).EnrollCamInGroup(inValue);
            camEnrolledGuid = retVal.camEnrolledGuid;
            return retVal.EnrollCamInGroupResult;
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.EnrollCamInGroupResponse> EnrollCamInGroupAsync(CareBedWebcamLib.CamControlService.EnrollCamInGroupRequest request) {
            return base.Channel.EnrollCamInGroupAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CareBedWebcamLib.CamControlService.GetCamInfoResponse CareBedWebcamLib.CamControlService.ICamControlService.GetCamInfo(CareBedWebcamLib.CamControlService.GetCamInfoRequest request) {
            return base.Channel.GetCamInfo(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo GetCamInfo(string camId, out string ftpUser, out string ftpPwd, out string groupName, out bool isRecMotion) {
            CareBedWebcamLib.CamControlService.GetCamInfoRequest inValue = new CareBedWebcamLib.CamControlService.GetCamInfoRequest();
            inValue.camId = camId;
            CareBedWebcamLib.CamControlService.GetCamInfoResponse retVal = ((CareBedWebcamLib.CamControlService.ICamControlService)(this)).GetCamInfo(inValue);
            ftpUser = retVal.ftpUser;
            ftpPwd = retVal.ftpPwd;
            groupName = retVal.groupName;
            isRecMotion = retVal.isRecMotion;
            return retVal.GetCamInfoResult;
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.GetCamInfoResponse> GetCamInfoAsync(CareBedWebcamLib.CamControlService.GetCamInfoRequest request) {
            return base.Channel.GetCamInfoAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CareBedWebcamLib.CamControlService.GetGroupSIDResponse CareBedWebcamLib.CamControlService.ICamControlService.GetGroupSID(CareBedWebcamLib.CamControlService.GetGroupSIDRequest request) {
            return base.Channel.GetGroupSID(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo GetGroupSID(string groupName, out string groupSID) {
            CareBedWebcamLib.CamControlService.GetGroupSIDRequest inValue = new CareBedWebcamLib.CamControlService.GetGroupSIDRequest();
            inValue.groupName = groupName;
            CareBedWebcamLib.CamControlService.GetGroupSIDResponse retVal = ((CareBedWebcamLib.CamControlService.ICamControlService)(this)).GetGroupSID(inValue);
            groupSID = retVal.groupSID;
            return retVal.GetGroupSIDResult;
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.GetGroupSIDResponse> GetGroupSIDAsync(CareBedWebcamLib.CamControlService.GetGroupSIDRequest request) {
            return base.Channel.GetGroupSIDAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CareBedWebcamLib.CamControlService.GetCamInfoByGuidResponse CareBedWebcamLib.CamControlService.ICamControlService.GetCamInfoByGuid(CareBedWebcamLib.CamControlService.GetCamInfoByGuidRequest request) {
            return base.Channel.GetCamInfoByGuid(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo GetCamInfoByGuid(System.Guid camGuid, out string camId, out string ftpUser, out string ftpPwd, out string groupName, out bool isRecMotion) {
            CareBedWebcamLib.CamControlService.GetCamInfoByGuidRequest inValue = new CareBedWebcamLib.CamControlService.GetCamInfoByGuidRequest();
            inValue.camGuid = camGuid;
            CareBedWebcamLib.CamControlService.GetCamInfoByGuidResponse retVal = ((CareBedWebcamLib.CamControlService.ICamControlService)(this)).GetCamInfoByGuid(inValue);
            camId = retVal.camId;
            ftpUser = retVal.ftpUser;
            ftpPwd = retVal.ftpPwd;
            groupName = retVal.groupName;
            isRecMotion = retVal.isRecMotion;
            return retVal.GetCamInfoByGuidResult;
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.GetCamInfoByGuidResponse> GetCamInfoByGuidAsync(CareBedWebcamLib.CamControlService.GetCamInfoByGuidRequest request) {
            return base.Channel.GetCamInfoByGuidAsync(request);
        }
        
        public CareBedWebcamLib.CamControlService.ResultInfo SetCamFtpCredentials(string camId) {
            return base.Channel.SetCamFtpCredentials(camId);
        }
        
        public System.Threading.Tasks.Task<CareBedWebcamLib.CamControlService.ResultInfo> SetCamFtpCredentialsAsync(string camId) {
            return base.Channel.SetCamFtpCredentialsAsync(camId);
        }
    }
}
