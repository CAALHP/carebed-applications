﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CareBedDatatypeLib;

namespace CareBedWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class CareBedService : ICareBedDataContract
    {

        private static readonly List<MonitorDataObject> MonitorList = new List<MonitorDataObject>();

        public List<MonitorDataObject> GetAllBedStatus()
        {
            lock (((ICollection)MonitorList).SyncRoot)
            {
                return MonitorList;
            }
        }

        public void UploadBedStatus(MonitorDataObject measurement)
        {

            lock (((ICollection)MonitorList).SyncRoot)
            {
                var indexOfItem = MonitorList.FindIndex(c => c.MachineId == measurement.MachineId);
                if (indexOfItem != -1)
                    MonitorList[indexOfItem] = measurement;
                else
                    MonitorList.Add(measurement);
            }

        }
    }
}
