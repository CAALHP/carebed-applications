﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace CareBedSurveyer.ViewModels
{
    [Serializable]
    public class SurveillanceImageViewModel: BaseViewModel
    {
        private BitmapImage _image;
        private DateTime _timeStamp;

        public SurveillanceImageViewModel(BitmapImage img, DateTime timeStamp)
        {
            Image = img;
            TimeStamp = timeStamp;
        }

        public BitmapImage Image
        {
            get { return _image; }
            set
            {
                if (Equals(value, _image)) return;
                _image = value;
                NotifyOfPropertyChange(() => Image);
            }
        }

        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set
            {
                if (value.Equals(_timeStamp)) return;
                _timeStamp = value;
                NotifyOfPropertyChange(() => TimeStamp);
            }
        }
    }
}
