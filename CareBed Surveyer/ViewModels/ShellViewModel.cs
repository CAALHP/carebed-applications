

using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using CAALHP.SOA.ICE.ClientAdapters;
using Caliburn.Micro;
using CareBedSurveyer.EventAggregators;
using CareBedSurveyer.EventMessages;

namespace CareBedSurveyer.ViewModels {

    [Export(typeof(IShell))]
    public class ShellViewModel : Screen, IShell, IHandle<BaseMessage>
    {
        private CareBedSurveyerImplementation _caalhpConctact;
        private AppAdapter _adapter;
        const string endpoint = "localhost";
        private bool _connectedToCaalhp;
        private bool _isWindowEnabled;
        private string _header;
        private BedSureillanceViewModel _mainView;
        private System.Timers.Timer _statusTimer = new System.Timers.Timer(30000);
        private string _statusText;


        public ShellViewModel()
        {

            Task.Run(() =>
            {
                try
                {
                    _caalhpConctact = new CareBedSurveyerImplementation(this);
                    _adapter = new AppAdapter(endpoint, _caalhpConctact);

                    _connectedToCaalhp = true;

                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                    StatusText = "Failed to connect to Caalhp";
                    _connectedToCaalhp = false;
                }
            });

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            IsWindowEnabled = true;

            Header = "CareBed Surveyer";

            _statusTimer.Elapsed += StatusTimerElapsed;
            

            MainView = new BedSureillanceViewModel();
        }

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            //todo something here to log errors


            ////e.Handled = true;
            ////MessageBox.Show(e.Exception.Message);

            ////forces thread to change to da-DK format for storing log.
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");

            ////writes unhandled exceptions into logfil and closes down application.
            //var path = @"c:\Log\Exceptions\";

            ////  if (Directory.Exists(path))
            //Directory.CreateDirectory(path);


            //string filename = path + DateTime.Now.Date.ToShortDateString() + ".txt";
            ////writes unhandled exceptions into logfil and closes down application.
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename, true))
            //{
            //    file.Write(DateTime.Now.ToShortTimeString() + " ");
            //    file.WriteLine(e.Exception.ToString());

            //}
        }

        void StatusTimerElapsed(object sender, ElapsedEventArgs e)
        {
            StatusText = null;
        }

        public BedSureillanceViewModel MainView
        {
            get { return _mainView; }
            set
            {
                if (Equals(value, _mainView)) return;
                _mainView = value;
                NotifyOfPropertyChange(() => MainView);
            }
        }

        public string Header
        {
            get { return _header; }
            set
            {
                if (value == _header) return;
                _header = value;
                NotifyOfPropertyChange(() => Header);
            }
        }

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (value == _statusText) return;
                _statusText = value;
                NotifyOfPropertyChange(() => StatusText);

                if (string.IsNullOrEmpty(value)) return;
                _statusTimer.Start();

            }
        }

        public void WindowClose(object sender, CancelEventArgs e)
        {
            if (_connectedToCaalhp)
            {
                e.Cancel = true;
                //TODO check if this is needed 
                IsWindowEnabled = false;
            }
        }

        public bool IsWindowEnabled
        {
            get { return _isWindowEnabled; }
            set
            {
                if (value.Equals(_isWindowEnabled)) return;
                _isWindowEnabled = value;
                NotifyOfPropertyChange(() => IsWindowEnabled);
            }
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is empty by design
        }

        private void HandleMessage(StatusMessage o)
        {
            StatusText = o.Message;
        }
    }
}