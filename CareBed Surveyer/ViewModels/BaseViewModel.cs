﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace CareBedSurveyer.ViewModels
{
    [Export(typeof(BaseViewModel))]
    public abstract class BaseViewModel : Screen
    {
        protected BaseViewModel()
        {

        }


        /// <summary>
        /// updates the controls of the current view to syncronice with viewmodel
        /// Should be used if resources are used for localization in the viewmodel.
        /// </summary>
        public void UpdateView()
        {
            NotifyOfPropertyChange(string.Empty);
        }
    }
}
