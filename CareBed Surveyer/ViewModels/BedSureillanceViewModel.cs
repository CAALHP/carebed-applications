﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Caliburn.Micro;
using CareBedDatatypeLib;
using CareBedSurveyer.EventAggregators;
using CareBedSurveyer.EventMessages;
using CareBedSurveyer.Model;
using CareBedWebcamLib;

namespace CareBedSurveyer.ViewModels
{
    public sealed class BedSureillanceViewModel : Conductor<Screen>.Collection.AllActive
    {
        //private List<UserViewModel> _listOfUsers;
        private IDataService _dataService = new DataService();
        private List<UserViewModel> _monitorList = new List<UserViewModel>();
        private const string Endpoint = "http://cam.cw.consulo.dk:5080/CamImageService/";
        private const string Groupname = "cwlab"; //todo consider getting this information from the monitordataobject.
        //private const string Camid = "CY000001";
        private readonly CareCam _cam;

        private DispatcherTimer _updateDataTimer;

        public BedSureillanceViewModel()
        {

            _cam = new CareCam(Endpoint, Groupname);

            SetupTimer();

            UpdateMonitorDataFromServer();

            // var one = new UserViewModel(cam, camid);

            //var one = new UserViewModel(_cam, Camid)
            //   {
            //       AvarageWeight = 80,
            //       UserName = "Bente Nielsel",
            //       InBedStatus = true,
            //       Quality = SleepQuality.Medium,
            //       RoomId = "101",
            //       Weight = 80,
            //       LastWeightTimeStamp = DateTime.Now
            //   };

            //var five = new UserViewModel() { AvarageWeight = 80, UserName = "Bente Nielsel", InBedStatus = true, Quality = SleepQuality.Low, RoomId = "103", Weight = 70, LastWeightTimeStamp = DateTime.Now };
            //var two = new UserViewModel() { AvarageWeight = 80, UserName = "Bente Nielsel", InBedStatus = false, Quality = SleepQuality.Medium, RoomId = "80", Weight = 60, LastWeightTimeStamp = DateTime.Now };
            //var tree = new UserViewModel() { AvarageWeight = 80, UserName = "Bente Nielsel", InBedStatus = true, Quality = SleepQuality.High, RoomId = "121", Weight = 50, LastWeightTimeStamp = DateTime.Now };
            //var four = new UserViewModel() { AvarageWeight = 80, UserName = "Bente Nielsel", InBedStatus = false, Quality = SleepQuality.Medium, RoomId = "15", Weight = 40, LastWeightTimeStamp = DateTime.Now };

            //ActivateItem(one);
            //ActivateItem(two);
            //ActivateItem(tree);
            //ActivateItem(four);
            //ActivateItem(five);
        }

        private void SetupTimer()
        {
           // int updateIntervel;
           // int.TryParse(ConfigurationManager.AppSettings.Get("updateInterval"), out updateIntervel);


            _updateDataTimer = new DispatcherTimer();
            _updateDataTimer.Tick += UpdateDataTimerOnTick;
            _updateDataTimer.Interval = new TimeSpan(0, 0, 5);
            _updateDataTimer.Start();
        }

        private void UpdateDataTimerOnTick(object sender, EventArgs e)
        {
            //Dispatcher.BeginInvoke(new Action(GetPictureFromService));

            UpdateMonitorDataFromServer();

        }

        private void UpdateMonitorDataFromServer()
        {
            List<MonitorDataObject> dataList = null;

            try
            {
                dataList = _dataService.GetMonitorStatus();
            }
            catch (Exception)
            {
                var msg = "Failed to update from server";
                ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() { Message = msg });

            }

            if (dataList == null || dataList.Count == 0) return;

            foreach (var monitorDataObject in dataList)
            {
                var sleepQuality = GetSleepQuality(monitorDataObject.SleepQuality);

                var indexOfItem = _monitorList.FindIndex(c => c.MonitorId == monitorDataObject.MachineId);

                if (indexOfItem != -1)
                {
                    var itemtoUpdate = _monitorList[indexOfItem];

                    itemtoUpdate.UserName = monitorDataObject.UserName;
                    itemtoUpdate.RoomId = monitorDataObject.RoomId;
                    itemtoUpdate.AvarageWeight = monitorDataObject.AvarageWeight;
                    itemtoUpdate.InBedStatus = monitorDataObject.BedStatus;
                    itemtoUpdate.Quality = sleepQuality;
                    itemtoUpdate.Weight = monitorDataObject.LastWeighing.Value;
                    itemtoUpdate.LastWeightTimeStamp = monitorDataObject.LastWeighing.TimeStamp;
                }

                else
                {
                    var camId = monitorDataObject.CameraId;

                    var newUserView = new UserViewModel(_cam, camId, monitorDataObject.MachineId)
                    {
                        UserName = monitorDataObject.UserName,
                        RoomId = monitorDataObject.RoomId,
                        AvarageWeight = monitorDataObject.AvarageWeight,
                        InBedStatus = monitorDataObject.BedStatus,
                        Quality = sleepQuality,
                        Weight = monitorDataObject.LastWeighing.Value,
                        LastWeightTimeStamp = monitorDataObject.LastWeighing.TimeStamp
                    };

                    _monitorList.Add(newUserView);
                    ActivateItem(newUserView);
                }
            }

        }

        private SleepQuality GetSleepQuality(int sleepQuality)
        {
            if (sleepQuality < 30)
                return SleepQuality.Low;
            if (sleepQuality < 70)
                return SleepQuality.Medium;

            return SleepQuality.High;
        }


        //public List<UserViewModel> ListOfUsers
        //{
        //    get { return _listOfUsers; }
        //    set
        //    {
        //        if (Equals(value, _listOfUsers)) return;
        //        _listOfUsers = value;
        //        NotifyOfPropertyChange(() => ListOfUsers);
        //    }
        //}
    }
}
