﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Serialization;
using CAALHP.Events.Types;
using Caliburn.Micro;
using CareBedDatatypeLib;
using CareBedSurveyer.Model;
using CareBedWebcamLib;
using Action = System.Action;

namespace CareBedSurveyer.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        private CareCam _camService;
        private string _camId;
        //private Timer _updateDataTimer;
        private DispatcherTimer _updateDataTimer;

        private string _roomId;
        private string _userName;
        private SleepQuality _quality;
        private double _avarageWeight;
        private double _weight;
        private DateTime _lastWeightTimeStamp;
        //private IObservableCollection<SurveillanceImageViewModel> _imageList = new BindableCollection<SurveillanceImageViewModel>();
        //private List<SurveillanceImageViewModel> _initialImageList;
        private bool _inBedStatus;
        private bool _isPicturesDisplayed;
        private IObservableCollection<SurveillanceImageViewModel> _imageList;// = new BindableCollection<SurveillanceImageViewModel>();
        private IObservableCollection<SurveillanceImageViewModel> _initialImageList = new BindableCollection<SurveillanceImageViewModel>();
        private readonly string _filename;
        private readonly string _userDir;

        private static object _fileLock = new object();


        public UserViewModel(CareCam camService, string camId, string monitorId)
        {
            MonitorId = monitorId;

            _userDir = Utility.AssemblyDirectory + "\\Data\\" + MonitorId;
            //_filename = dirname + camId + ".dat";
            InitList();


            _camService = camService;
            _camId = camId;

            IsPicturesDisplayed = false;

            //_initialImageList = new List<SurveillanceImageViewModel>()
            //    {
            //        new SurveillanceImageViewModel(new BitmapImage( new Uri( Utility.AssemblyDirectory + @"\Images\TestImg.jpg")), DateTime.Now), 
            //        new SurveillanceImageViewModel(new BitmapImage( new Uri( Utility.AssemblyDirectory + @"\Images\TestImg.jpg")), DateTime.Now), 
            //        new SurveillanceImageViewModel(new BitmapImage( new Uri( Utility.AssemblyDirectory + @"\Images\TestImg.jpg")), DateTime.Now)
            //    };



            //GetPictureFromService();

            SetupTimer();
            //GetPictureFromService();
            //GetPictureFromService();
        }

        public string MonitorId { get; private set; }

        public bool IsPicturesDisplayed
        {
            get { return _isPicturesDisplayed; }
            set
            {
                if (value.Equals(_isPicturesDisplayed)) return;
                _isPicturesDisplayed = value;
                NotifyOfPropertyChange(() => IsPicturesDisplayed);
            }
        }


        public bool InBedStatus
        {
            get { return _inBedStatus; }
            set
            {
                if (value.Equals(_inBedStatus)) return;
                _inBedStatus = value;
                NotifyOfPropertyChange(() => InBedStatus);
            }
        }

        public string RoomId
        {
            get { return _roomId; }
            set
            {
                if (value == _roomId) return;
                _roomId = value;
                NotifyOfPropertyChange(() => RoomId);
            }
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                if (value == _userName) return;
                _userName = value;
                NotifyOfPropertyChange(() => UserName);
            }
        }

        public SleepQuality Quality
        {
            get { return _quality; }
            set
            {
                if (value == _quality) return;
                _quality = value;
                NotifyOfPropertyChange(() => Quality);
            }
        }

        public double AvarageWeight
        {
            get { return _avarageWeight; }
            set
            {
                if (value.Equals(_avarageWeight)) return;
                _avarageWeight = value;
                NotifyOfPropertyChange(() => AvarageWeight);
            }
        }

        public double Weight
        {
            get { return _weight; }
            set
            {
                if (value.Equals(_weight)) return;
                _weight = value;
                NotifyOfPropertyChange(() => Weight);
            }
        }

        public DateTime LastWeightTimeStamp
        {
            get { return _lastWeightTimeStamp; }
            set
            {
                if (value == _lastWeightTimeStamp) return;
                _lastWeightTimeStamp = value;
                NotifyOfPropertyChange(() => LastWeightTimeStamp);
            }
        }

        //public List<SurveillanceImageViewModel> ImageList
        //{
        //    get
        //    {
        //        return _imageList;
        //        //return IsPicturesDisplayed ? _imageList : null;
        //    }
        //    set
        //    {
        //        if (Equals(value, _imageList)) return;
        //        _imageList = value;
        //        NotifyOfPropertyChange(() => ImageList);
        //    }
        //}

        public IObservableCollection<SurveillanceImageViewModel> ImageList
        {
            get { return _imageList; }
            set
            {
                if (Equals(value, _imageList)) return;
                _imageList = value;
                NotifyOfPropertyChange(() => ImageList);
            }
        }

        public void TogglePicture()
        {
            if (ImageList == null)
                ImageList = _initialImageList;

            IsPicturesDisplayed = !IsPicturesDisplayed;

            //Dispatcher.BeginInvoke(new Action(this.DisplayTime), DispatcherPriority.Background);
        }

        //private void SetupTimer()
        //{

        //    double updateIntervel;
        //    double.TryParse(ConfigurationManager.AppSettings.Get("updateInterval"), out updateIntervel);

        //    //TODO removed this for testing only

        //    _updateDataTimer = new Timer(updateIntervel);
        //    _updateDataTimer.Elapsed += UpdateDataTimerOnTick;
        //    _updateDataTimer.Start();
        //}

        private void SetupTimer()
        {
            int updateIntervel;
            int.TryParse(ConfigurationManager.AppSettings.Get("updateInterval"), out updateIntervel);
            //TODO  Set propper update time

            _updateDataTimer = new DispatcherTimer();
            _updateDataTimer.Tick += UpdateDataTimerOnTick;
            _updateDataTimer.Interval = new TimeSpan(0, 0, 10);
            _updateDataTimer.Start();
        }

        private void UpdateDataTimerOnTick(object sender, EventArgs e)
        {
            //Dispatcher.BeginInvoke(new Action(GetPictureFromService));

            GetPictureFromService();



        }

        private void GetPictureFromService()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {

                try
                {
                    var img = _camService.GetLatestImage(_camId);
                    //TODO get a propper timestamp form the service. somehow.
                    var timestamp = DateTime.Now;

                    WritetoFile(img, timestamp);

                    var imgViewModel = new SurveillanceImageViewModel((BitmapImage)img, timestamp);

                    //if (ImageList == null)
                    //    ImageList = _initialImageList;

                    if (_initialImageList.Count == 3)
                        _initialImageList.RemoveAt(0);

                    _initialImageList.Add(imgViewModel);

                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                    //TODO do some notification if this fails.
                    // throw;
                }
            }));

        }



        private void InitList()
        {
            _initialImageList = new BindableCollection<SurveillanceImageViewModel>();

            try
            {
                if (Directory.Exists(_userDir))
                {
                    AddImagesFromFiles(_initialImageList);
                }
                else
                {
                    Directory.CreateDirectory(_userDir);
                    return;
                }

                //if (File.Exists(_filename))
                //    ReadFromFile();
                //else
                //{
                //    WritetoFile();
                //}
            }
            catch (Exception e)
            {

                _initialImageList = new BindableCollection<SurveillanceImageViewModel>();
            }
        }

        private void WritetoFile(BitmapSource img, DateTime timestamp)
        {
            try
            {
                var files = Directory.GetFiles(_userDir);
                if (files.Count() == 3)
                {
                    string oldest = "";
                    DateTime oldestWriteTime = new DateTime();
                    foreach (var filepath in files)
                    {
                        var fileLastWrite = File.GetLastWriteTime(filepath);

                        if (string.IsNullOrEmpty(oldest))
                        {
                          //  oldestWriteTime = fileLastWrite;
                            oldest = filepath;
                        }
                        else
                        {
                            if (File.GetLastWriteTime(oldest) > fileLastWrite)
                            {
                               // oldestWriteTime = fileLastWrite;
                                oldest = filepath;
                            }
                        }

                    }

                    File.Delete(oldest);
                }
            }
            catch (Exception)
            {
                
            }


            var tStamp = timestamp.ToString(CultureInfo.InvariantCulture);
            tStamp = tStamp.Replace(':', '+');
            tStamp = tStamp.Replace('/', 'k');
            var filePath = _userDir + "\\" + tStamp + ".jpg";

            //var oldFiles = new DirectoryInfo(_userDir).fi

            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapImage)img));

            using (var filestream = new FileStream(filePath, FileMode.Create))
            {
                encoder.Save(filestream);
            }

        }

        private void AddImagesFromFiles(IObservableCollection<SurveillanceImageViewModel> initialImageList)
        {
            var files = Directory.GetFiles(_userDir);

            foreach (var fileName in files)
            {
                //var photoPath = Utils.ImagePath + PhotoLink;
                BitmapImage img;
                try
                {
                    img = new BitmapImage();
                    img.BeginInit();
                    img.CacheOption = BitmapCacheOption.OnLoad;
                    img.UriSource = new Uri(fileName, UriKind.Absolute);
                    img.EndInit();
                    img.Freeze();
                }
                catch (Exception)
                {
                    img = null;
                }

                //var timeStamp = File.GetLastWriteTime(fileName);

                //TODO change to use file name to take time
                if (img == null) continue;

                var timeStamp = fileName.Split('\\').Last();
                timeStamp = timeStamp.Remove(timeStamp.Length - 4);
                timeStamp = timeStamp.Replace('+', ':');
                timeStamp = timeStamp.Replace('k', '/');

                DateTime time = DateTime.ParseExact(timeStamp, "dd-MM-yyyy HH:mm:ss",
                                                    System.Globalization.CultureInfo.InvariantCulture);

                var imgViewModel = new SurveillanceImageViewModel(img, time);
                initialImageList.Add(imgViewModel);
            }
        }


        //private void ReadFromFile()
        //{

        //    try
        //    {
        //        lock (_fileLock)
        //        {
        //            using (Stream stream = File.Open(_filename, FileMode.Open))
        //            {
        //                var bin = new BinaryFormatter();
        //                stream.Position = 0;
        //                _initialImageList = (BindableCollection<SurveillanceImageViewModel>)bin.Deserialize(stream);
        //            }
        //        }

        //    }
        //    catch (IOException ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void WritetoFile()
        //{
        //    //try
        //    //{

        //    //    lock (_fileLock)
        //    //    {
        //    //        using (Stream stream = File.Open(_filename, FileMode.Create))
        //    //        {
        //    //            var bin = new BinaryFormatter();
        //    //            bin.Serialize(stream, _initialImageList);
        //    //        }
        //    //    }

        //    //}
        //    //catch (IOException)
        //    //{
        //    //}



        //}
    }
}
