﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using CareBedDatatypeLib;

namespace CareBedSurveyer.Model
{
    public class DataService : IDataService
    {
        private ICareBedDataContract _bedService;

        public DataService()
        {
            try
            {
                //var maxsize = 2147483648;
                //binding
                var binding = new BasicHttpBinding();
                binding.MaxBufferSize = 2138483646;
                binding.MaxReceivedMessageSize = 2138483646;
                binding.SendTimeout = new System.TimeSpan(1, 0, 0, 0);
                binding.ReceiveTimeout = new System.TimeSpan(1, 0, 0, 0);
                //var binding = new BasicHttpsBinding();
                //endpoint
                var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("webServiceAdress"));
                //<add key="marketplaceAddress" value="http://192.168.1.100/Marketplaceservice/CareStoreMarketplaceWcfService.svc"/>

                //channelfactory
                var channelFactory = new ChannelFactory<ICareBedDataContract>(binding, endpoint);

                _bedService = channelFactory.CreateChannel();
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public List<MonitorDataObject> GetMonitorStatus()
        {
            var result = _bedService.GetAllBedStatus();
            return result;
        }
    }
}
