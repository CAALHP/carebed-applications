﻿using System.Collections.Generic;
using CareBedDatatypeLib;

namespace CareBedSurveyer.Model
{
    public interface IDataService
    {
        List<MonitorDataObject> GetMonitorStatus();
    }
}
