﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CareBedSurveyer.ViewModels;
using CareBedSurveyer.Views;

namespace CareBedSurveyer
{
    public class CareBedSurveyerImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private const string Appname = "CareBed Surveyer";
        private ShellViewModel _shell;

        public CareBedSurveyerImplementation(ShellViewModel shell)
        {
            _shell = shell;
        }

        public string GetName()
        {
            return Appname;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
        }

        public void Show()
        {
            //TODO do some showmagic here :D
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(GetName()))
            {
                //Show homescreen
                Show();
            }
        }
    }
}
