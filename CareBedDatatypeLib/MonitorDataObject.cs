﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CareBedDatatypeLib
{
    [Serializable]
    [DataContract]
    public class MonitorDataObject
    {

        [DataMember]
        public string MachineId { get; set; }

        [DataMember]
        public string RoomId { get; set; }

        [DataMember]
        public string CameraId { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool BedStatus { get; set; }

        [DataMember]
        public Measurement LastWeighing { get; set; }

        [DataMember]
        public double AvarageWeight { get; set; }

        [DataMember]
        public int SleepQuality { get; set; }

    }
}
