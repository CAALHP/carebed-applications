﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CareBedDatatypeLib
{
    [Serializable]
    [DataContract]
    public class Measurement
    {
        [DataMember]
        public double Value { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }
    }
}
