﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CareBedDatatypeLib
{

    [ServiceContract]
    public interface ICareBedDataContract
    {
        [OperationContract]
        List<MonitorDataObject> GetAllBedStatus();

        [OperationContract]
        void UploadBedStatus(MonitorDataObject measurement);
    }
}
